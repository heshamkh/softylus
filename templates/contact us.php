<?php
/**
 * template Name: contact_us
 * @package Bulmapress
 */

get_header(); ?>
<div id="primary" class="site-content has-background-white" xmlns="http://www.w3.org/1999/html">
    <div id="content" role="main" style="overflow: hidden;">
        <section>
        <div class="container">
            <div class="columns m-6 flip">

                <div class="column is-6 flip-down">
                    <span class="small-header"></span>
                    <h1 class="my-4 is-capitalized is-size-2 is-size-4-mobile is-family-softylus-black has-text-black"></h1>
                    <p class="is-family-softylus-reg is-size-4 is-size-6-mobile">
                    </p>
                     <span class="small-header">CONTACT US</span>
                    <h1 class="mb-3 line-height is-uppercase is-size-2-mobile is-size-1 is-family-softylus-black">Let Us Know What Your<span class="has-text-red">Project</span> is About!</h1>
                    <p class="is-family-softylus-reg line-height-p is-size-5">
                        Share the details of your project with us so we can handle the rest for you.
                    </p>
                </div>
                <div class="column px-6 is-6 flip-up">
                        <figure class="image">
                            <img src="https://s1.softylus.com/wp-content/uploads/2020/12/contactimg.png">
                        </figure>

                </div>

            </div>
        </div></section>
        <section class="aboutSection is-vcentered is-hidden-tablet my-6 p-6">
            <div class="container is-vcentered">
                <div class="columns has-text-centered is-vcentered is-multiline is-mobile">

                    <div class="aboutCol column is-vcentered">
                        <h1  class=" is-capitalized is-size-2 is-size-4-mobile is-family-softylus-black has-text-black is-vcentered has-text-centered">Create Your Next Project With Our Professional Services </h1>
                        <a href="<?php echo esc_attr( esc_url( get_page_link( 189 ) ) ) ?>">
                        <button class="btn is-family-softylus-bold has-background-red is-size-6 has-text-white is-uppercase">
                            About us
                        </button></a>
                    </div>

                </div>
            </div>
        </section>
        <section class="container px-6-mobile">
            <div class="columns flip">
                <div class="column is-1 is-hidden-mobile">

                </div>
                <div class="column is-4 py-6-mobile ">
                    <span class="small-header">CONTACT US</span>
                    <h1 class="my-4 is-capitalized is-size-2 is-size-4-mobile is-family-softylus-black has-text-black">Get In Touch With Us
                    </h1>
                    <div class="columns is-mobile my-2">
                        <div class="column is-one-fifth ">
                            <figure >
                                <img class="image is-m-auto" src="https://s1.softylus.com/wp-content/uploads/2020/12/phone-icon.svg">
                            </figure>
                        </div>
                        <div class="column p-0">
                            <h2 class="is-family-softylus-light is-size-6 is-capitalized ">phone number</h2>
                            <p class="is-family-softylus-black is-size-6 is-capitalized ">00962 788 954 236</p>
                        </div>
                    </div>
                    <div class="columns is-mobile my-2">
                        <div class="column is-one-fifth ">
                            <figure >
                                <img class="image is-m-auto" src="https://s1.softylus.com/wp-content/uploads/2020/12/email-Icon.svg">
                            </figure>
                        </div>
                        <div class="column p-0">
                            <h2 class="is-family-softylus-light is-size-6 is-capitalized ">Email</h2>
                            <p class="is-family-softylus-black is-size-6 is-capitalized ">info@softylus.com</p>
                        </div>
                    </div>
                    <div class="columns is-mobile my-2">
                        <div class="column is-one-fifth ">
                            <figure >
                                <img class="image is-m-auto" src="https://s1.softylus.com/wp-content/uploads/2020/12/street-Icon.svg">
                            </figure>
                        </div>
                        <div class="column p-0">
                            <h2 class="is-family-softylus-light is-size-6 is-capitalized ">Map street</h2>
                            <p class="is-family-softylus-black is-size-6 is-capitalized ">00962 788 954 236</p>
                        </div>
                    </div>
                </div>
                <div class="ContactUsForm is-7 column is-rounded has-text-black m-3 flip-up">
                    <?php echo do_shortcode("[contact-form-7 id=\"171\" title=\"contact us\"]"); ?>
                </div>
            </div>
        </section>
        <section class="aboutSection is-vcentered my-6 is-hidden-mobile">
            <div class="container is-vcentered" style="height: 100%">
            <div class="columns has-text-centered is-vcentered pb-5 pt-6" style="height: 100%">
                <div class="aboutCol column mb-2 mt-5 is-half has-background-white is-vcentered " style="height: 100%">
                    <h1  class="mt-4 is-capitalized is-size-2 is-size-4-mobile is-family-softylus-black has-text-black is-vcentered has-text-centered" style="padding-top:24% !important">Get Started With Your Projects Now, And Plan For Your Future Ones Too! </h1>
                    <a href="<?php echo esc_attr( esc_url( get_page_link( 189 ) ) ) ?>" style="cursor: pointer">
                    <button class="btn mt-5 is-family-softylus-bold has-background-red is-size-6 has-text-white is-uppercase" >
                        About us
                    </button></a>
                </div>
                <div class="column">

                </div>
            </div>
    </div>
        </section>
        <section class="container mb-6" >
            <div class="columns mb-6 is-multiline">
                <div class="column has-text-centered is-12">
                    <h1 class="is-capitalized is-size-1 is-size-2-mobile is-family-softylus-black has-text-black">Frequently Asked Questions</h1>
                </div>
               <div class="column is-5 py-0-mobile">
                   <ul class="accordion mx-3">
                       <li>
                           <input  onclick=' $(this).parent().toggleClass("redAcc");' type="checkbox" checked>
                           <i class="accI"></i>
                           <h2 class="accH2"> What’s the average cost for developing web/mobile apps?
                           </h2>
                           <p class="accP">The pricing differs considering the details of your project, therefore fixed prices can’t be estimated. But once you reach out to us and discuss the details, we’ll be more than glad to enlighten you with the specific answer you’re looking for.
                           </p>
                       </li>
                       <li>
                           <input  type="checkbox" onclick=' $(this).parent().toggleClass("redAcc");' checked>
                           <i class="accI"></i>
                           <h2 class="accH2">How much time does it take to get my web/mobile app done?
                           </h2>
                           <p class="accP">Once we discuss your project with you, we’ll be able to share the estimated time frame needed to get the project done.
                           </p>
                       </li>
                       <li>
                           <input type="checkbox" onclick=' $(this).parent().toggleClass("redAcc");' checked>
                           <i class="accI"></i>
                           <h2 class="accH2">Does the development contract include domain hosting services?</h2>
                           <p class="accP">Yes upon request, considering we’re part of the hosting company Hostylus, we’re more than pleased to provide you with the suitable hosting plan that you need for great prices and discount codes.
                           </p>
                       </li>
                   </ul>
               </div>
                <div class="column is-2 is is-hidden-mobile">

                </div>
                <script
  src="https://code.jquery.com/jquery-3.5.1.slim.min.js"
  integrity="sha256-4+XzXVhsDmqanXGHaHvgh1gMQKX40OUvDEBTu8JcmNs="
  crossorigin="anonymous"></script>
                <div class="column is-5 py-0-mobile">
                    <ul class="accordion mx-3">
                        <li>
                            <input type="checkbox" onclick=' $(this).parent().toggleClass("redAcc");' checked>
                            <i class="accI"></i>
                            <h2 class="accH2 has-text-red">Do you cover maintenance/support contracts?</h2>
                            <p class="accP">Absolutely! All you have to do is get in touch with us and share your interest to have one. We’ll then provide you with the prices and further details.</p>
                        </li>
                        <li>
                            <input type="checkbox" onclick=' $(this).parent().toggleClass("redAcc");' checked>
                            <i class="accI"></i>
                            <h2 class="accH2 has-text-red"> Are your services available on weekends/vacations?</h2>
                            <p class="accP">If it’s for an urgent project of yours, we do our best to deliver what you need on time.</p>
                        </li>
                        <li>
                            <input type="checkbox" onclick=' $(this).parent().toggleClass("redAcc");' checked />
                            <i class="accI"></i>
                            <h2 class="accH2 has-text-red">Do you offer design revisions?</h2>
                            <p class="accP">Of course! We understand the different tastes of our clients. Therefore, we do our part to present you with various options till you decide on one that fully satisfies you.
                            </p>
                        </li>
                    </ul>
                </div>
            </div>
        </section>

    </div>
</div>
<?php get_footer(); ?>
