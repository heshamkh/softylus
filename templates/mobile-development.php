<?php
/**
 * template Name: mobile-development
 * @package Bulmapress
 */

get_header(); ?>
<!-- test -->
<div id="primary" class="site-content has-background-white" xmlns="http://www.w3.org/1999/html">
    <div id="content" role="main" style="overflow: hidden;">
        <section class="container mt-0  px-5-mobile" >
            <div class="columns is-vcentered flip">
                <div class="column is-half flip-down has-text-centered-mobile ">
                    <span class="small-header">MOBILE DEVELOPMENT</span>
                    <h1 class="mb-3 line-height is-uppercase is-size-2-mobile is-size-1 is-family-softylus-black">Providing You With Refined <span class="has-text-red">Mobile Designs</span></h1>
                  
                    <p class="is-family-softylus-reg is-size-5 px-6-mobile is-size-6-mobile">
                       Whatever goals that you have in mind, we pour our hearts and souls to get them done as they should, and because we don’t take our customers’ dreams lightly we put the efforts needed so you could have the best experience and polished designs.
                    </p>
                    <button class="btn mt-5 is-family-softylus-bold has-background-red is-size-6 has-text-white is-uppercase">
                        get started
                    </button>
                </div>
                <div class="column is-half flip-up">
                    <figure class="image">
                        <img class="is-m-auto p-5" src="https://s1.softylus.com/wp-content/uploads/2020/12/test-e1608117714254.png">
                    </figure>
                </div>
            </div>
        </section>
        <section class="section">
        <div class="container">
            <div class="columns ">
                <div class="column has-text-centered is-mobile is-full" >
                    <span class="small-header">How we help you?</span>
                    <h1 class="my-5 line-height is-size-2-mobile is-uppercase is-size-1 is-family-softylus-black">Our Features</h1>
                    <p style="line-height:1.4 ;" class="is-family-softylus-reg line-height-p my-5 px-6 is-size-5 is-size-6-mobile">
                    Regardless of the type of business you’re running, we employ up-to-date technologies with amazing features that enhance your mobile app performance on all devices
                    </p>
                </div>
            </div>
            <div class="columns mt-6 is-full  ">
                <div class="columns myBox creative bg-white">
                    <div class="column is-one-fifth ">
                        <figure class=" has-text-centered-mobile creativeM is-mobile">
                            <img class="image is-96x96 is-m-auto creativeImg" src="https://s1.softylus.com/wp-content/uploads/2020/12/creative-display2.svg">
                        </figure>
                    </div>
                    <div class="column has-text-centered-mobile is-mobile ">
                        <h2 class="is-family-softylus-black my-3 font-24 has-text-centered-mobile is-uppercase is-mobile ">Creative App Display</h2>
                        <p class="pr-3">Overlooking outdated designs, and building up visually satisfying designs</p>
                        <button class="button mt-5 px-6 font-weight-smaill is-red is-uppercase is-rounded ">Read More</button>
                    </div>
                </div>

                <div class="column is-1">


                </div>
                <div class="columns myBox custom bg-white">
                    <div class="column  is-one-fifth ">
                        <figure class="has-text-centered-mobile customM is-mobile">
                            <img class="image is-96x96 is-m-auto customImg" src="https://s1.softylus.com/wp-content/uploads/2020/12/cusomization2.svg">
                        </figure>
                    </div>
                    <div class="column has-text-centered-mobile is-mobile ">
                        <h2 class="is-family-softylus-black my-3 font-24 has-text-centered-mobile is-uppercase is-mobile ">Unique and Custom Options </h2>
                        <p class="pr-3">Built in a way that especially caters to all of your needs and desires</p>
                        <button class="button mt-5 px-6 font-weight-smaill is-red is-uppercase is-rounded ">Read More</button>
                    </div>
                </div>

            </div>
            <!-- end the section 1  -->

            <div class="columns mt-6 is-full  ">
                <div class="columns myBox secure bg-white">
                    <div class="column is-one-fifth ">
                        <figure class=" has-text-centered-mobile is-mobile">
                            <img class="image is-96x96 is-m-auto secureImg"  src="https://s1.softylus.com/wp-content/uploads/2020/12/secure2.svg">
                        </figure>
                    </div>
                    <div class="column has-text-centered-mobile is-mobile ">
                        <h2 class="is-family-softylus-black my-3 font-24 has-text-centered-mobile is-uppercase is-mobile ">Secure and Perfect Integration</h2>
                        <p  class="pr-3">We keep an eye on the latest security measures so no sensitive data is leaked</p>
                        <button class="button mt-5 px-6 font-weight-smaill is-red is-uppercase is-rounded ">Read More</button>
                    </div>
                </div>
                <div class="column is-1">


                </div>
                <div class="columns myBox performance bg-white">
                    <div class="column is-one-fifth ">
                        <figure class="has-text-centered-mobile is-mobile">
                            <img  class="image is-96x96 is-m-auto performanceImg" src="https://s1.softylus.com/wp-content/uploads/2020/12/performance2.svg">
                        </figure>
                    </div>
                    <div class="column has-text-centered-mobile is-mobile ">
                        <h2 class="is-family-softylus-black my-3 has-text-centered-mobile is-mobile is-uppercase font-24">High Performance </h2>
                        <p class="pr-3">Compelling features that best suit all types of apps to comply with the users’ expectations</p>
                        <button class="button mt-5 px-6 font-weight-smaill is-red is-uppercase is-rounded ">Read More</button>
                    </div>
                </div>
            </div>
        </div>
    </section>
        <section class="container" >
            <div class="columns py-6-desktop p-0-mobile is-vcentered">
                <div class="column is-half px-6">
                    <figure class="image ">
                        <img src="https://s1.softylus.com/wp-content/uploads/2020/12/test-1.png">
                    </figure>
                </div>
                <div class="column is-half px-6-mobile has-text-centered-mobile" >
                    <h2 class="mb-4 is-uppercase line-height is-size-1 is-size-2-mobile is-family-softylus-black">powerful application for <span class="has-text-red">you</span></h2>
                    <p class="is-family-softylus-reg is-size-5">Each of our applications is your own kind of powerful, thanks to our superb team’s research and delivery skills that will get you the best interaction with your application users. We do that while making sure you’re included in every step of the game. </p>
                </div>
            </div>

        </section>
        <section class="TakeControl my-6">
            <div style="height: 100%;"class="columns is-vcentered is-multiline">
                <div class="column is-6 p-0">
                    <figure class="image is-hidden-tablet ">
                        <img src="https://s1.softylus.com/wp-content/uploads/2020/12/workout-tracker-app.png">
                    </figure>
                </div>
                <div class="column is-6 is-hidden-mobile">
                </div>
                <div class="column is-6 is-hidden-mobile">
                </div>
                <div class="column is-6-desktop px-6-mobile  has-text-centered-mobile">
                    <h2 class="mb-4 line-height is-uppercase is-size-1 is-size-2-mobile is-family-softylus-black has-text-centered-mobile">Simplified User <span class="has-text-red"> Interface</span></h2>
                    <p class="is-family-softylus-reg is-size-6">Your user’s experience on the app matters but so does the aesthetics that come with it. We make sure to cover both so that no complexities are found at any stage.</p>
                </div>
            </div>
        </section>
        <section class="container" >
                <div class="columns is-vcentered flip">

                    <div class="column px-5 is-half  flip-down has-text-centered-mobile">
                        <h2 class="mb-4 line-height is-uppercase is-size-1 is-size-2-mobile is-family-softylus-black"> Hybrid Mobile  <span class="has-text-red">Applications</span></h2>
                        <p class="is-family-softylus-reg px-6-mobile pb-6-mobile is-size-6">Built-up from scratch to accommodate ios and android devices </p>
                    </div>
                    <div class="column px-6 is-half flip-up">
                        <figure class="image px-6">
                            <img src="https://s1.softylus.com/wp-content/uploads/2020/12/option2.png">
                        </figure>
                    </div>
                </div>

        </section>
        <section class="process container mb-6">
            <h2 class=" title px-3 is-3 has-text-black is-family-softylus-black  is-uppercase has-text-centered is-size-5-mobile">Our Mobile app development Process</h2>
            <hr class=" mb-6" style="width:15%;background-color:#CB0202;margin: auto;">
            <div class="columns is-multiline px-5">
                <div class="column is-4 has-text-centered">
                    <figure>
                        <img class="processIcon image" src="https://s1.softylus.com/wp-content/uploads/2020/12/strategy-1.svg">
                    </figure>
                    <h3 style="fon-size:15px !important;" class="is-family-softylus-black">1. Strategy</h3>
                    <span class="is-family-softylus-hairline">Tailoring a specific plan for each business</span>
                </div>
                <div class="column is-4 has-text-centered">
                    <figure>
                        <img class="processIcon image" src="https://s1.softylus.com/wp-content/uploads/2020/12/creative-design-red-1.svg">
                    </figure>
                    <h3  style="fon-size:15px !important;"  class="is-family-softylus-black">2. Design & User Testing</h3>
                    <span class="is-family-softylus-hairline">A visual draft that helps you imagine what the app will be like</span>
                </div>
                <div class="column is-4 has-text-centered">
                    <figure>
                        <img class="processIcon image" src="https://s1.softylus.com/wp-content/uploads/2020/12/coding-3-2.svg">
                    </figure>
                    <h3 style="fon-size:15px !important;"  class="is-family-softylus-black">3. Development</h3>
                    <span class="is-family-softylus-hairline">Getting the real job done once approved by the client</span>
                </div>

            </div>
        </section>
        <section class="container pt-6" >
            <div class="columns is-vcentered">
                <div class="column is-half has-text-centered-mobile">
                    <h2 class="mb-2 line-height is-uppercase is-size-1 is-size-2-mobile is-family-softylus-black">It’s not only about building apps, but paving ways for  <span class="has-text-red">your brands</span> </h2>
                    <p class="is-family-softylus-reg is-size-5">Mobile development is in our blood. We don’t just build apps, we create brand. Choosing us will be your best decision.</p>
                    <button class="contactBTN my-5 is-family-softylus-bold has-text-red is-size-6 is-uppercase">
                        lets take coffee with us
                    </button>
                </div>
                <div class="column is-half">
                    <figure class="image">
                        <img src="https://s1.softylus.com/wp-content/uploads/2020/12/call-mockup-img.png">
                    </figure>
                </div>
            </div>

        </section>
    </div>
</div>
<script
    src="https://code.jquery.com/jquery-3.5.1.slim.min.js"
    integrity="sha256-4+XzXVhsDmqanXGHaHvgh1gMQKX40OUvDEBTu8JcmNs="
    crossorigin="anonymous"></script>
<script src="https://s1.softylus.com/wp-content/themes/softylus/templates/replaceimg.js" >
</script>

<?php get_footer(); ?>

