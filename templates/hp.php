
<?php
/**
 * template Name: HP
 * @package Bulmapress
 */ 
 
get_header(); ?>
<!-- test -->
<div id="primary" class="site-content has-background-white">
    <div id="content" role="main">
    <!-- Flickity HTML init -->
<div class="gallery js-flickity"
  data-flickity-options='{ "wrapAround": true, "autoPlay": 6000, "fade": true, "prevNextButtons": false, "pageDots": false }'>
  <div class="gallery-cell slide-1 has-text-centered ">
  <section class="hero is-medium">

  <div class="hero-body">
    <div class="container has-text-centered">
    <h2 class="subtitle is-uppercase is-family-softylus-reg has-text-white">
        If It’s Hard On You,
      </h2>
      <h1 class="title is-uppercase is-family-softylus-black has-text-white mt-2">
       We Can <span class="has-text-red">Software</span> It For You!
      </h1>
      <h2 class="subtitle is-uppercase is-family-softylus-light has-text-white mb-6 mt-3 px-2">
          We Provide Professional Software Solutions For Your Business
      </h2>
      <div class="has-text-centered">
  <button class="button is-red is-uppercase is-rounded ">Read More</button>
  <button class="button is-danger is-inverted is-outlined is-uppercase is-rounded">Contact Us</button>
</div>
    </div>
  </div>
</section>


  </div>
  <div class="gallery-cell slide-2 has-text-centered"> 
      
  <section class="hero is-medium">
        <div class="hero-body">
            <div class="container has-text-centered">
            <h2 class="subtitle is-uppercase is-family-softylus-reg has-text-white">
                You Don’t Have to
            </h2>
            <h1 class="title is-uppercase is-family-softylus-black has-text-white mt-2">Run Out of <span class="has-text-red">Options</span> Anymore
            </h1>
            <h2 class="subtitle is-uppercase is-family-softylus-light has-text-white mb-6 mt-3 px-2">
                We’re Here to Run All The options for you
            </h2>
            <div class="has-text-centered">
        <button class="button is-red is-uppercase is-rounded ">Read More</button>
        <button class="button is-danger is-inverted is-outlined is-uppercase is-rounded">Contact Us</button>
        </div>
    </div>
  </div>
</section></div>
  <div class="gallery-cell slide-3 has-text-centered">  <section class="hero is-medium">

  <div class="hero-body">
    <div class="container has-text-centered">
    <h2 class="subtitle is-uppercase is-family-softylus-reg has-text-white">
        Our Vision Only
      </h2>
      <h1 class="title is-uppercase is-family-softylus-black has-text-white mt-2">
       Grows <span class="has-text-red">Bigger</span> With You
      </h1>
      <h2 class="subtitle is-uppercase is-family-softylus-light has-text-white mb-6 mt-3 p2-6">
          We’re More Than Thrilled to Collaborate With You
      </h2>
      <div class="has-text-centered">
  <button class="button is-red is-uppercase is-rounded ">Read More</button>
  <button class="button is-danger is-inverted is-outlined is-uppercase is-rounded">Contact Us</button>
</div>
    </div>
  </div>
</section></div>
  
</div>
        <section class="container who-we-are">
        <div  style="align-items: flex-end" class="columns  pt-6 is-multiline">
            <div class="column is-3">
                 <h2 class="title ml-5 pl-1 my-3-mobile has-text-centered-mobile  is-2 is-family-softylus-black ">Who <br class="is-hidden-mobile">Are We?</h2>
                   
            </div>
            <div class="column has-text-centered wwa-col is-6">
               <h2 class="title has-text-white text-inside-hex  is-family-softylus-bold"> SOFTYLUS </h2>

            </div>
            <div class="column is-3">

            </div>
            <div class="column px-6 py-1 has-text-centered-mobile  is-5">
               
                    <p class=" is-family-softylus-light">A software solution provider company specialized in web development, mobile development, design, and digital marketing. We take care of your project and keep in touch with you even when the job is done.
                    </p>
               
            </div>


</div> 
         <div class="HRcontainer"></div><hr class="is-hidden-mobile is-hidden-tablet-only line_diagonal1">
<div class = "columns ">
<div class="column  mt-2 has-text-centered is-half">

<figure class="mb-6 pb-4 "> 
    <img class="mb-6 mt-custom pb-4" style="width: 200px ;"src="https://ik.imagekit.io/softylus/imgonline-com-ua-shape-K9FuzJpqsfdXjjH_aUaAhAS1NViy.png"/>


</figure>


</div> 
<div style="margin-top: -100px; margin-bottom:100px; margin-left: 50px;" class="column is-half ">
    

<h2 class="mb-4 title px-3 is-2 has-text-red has-border-red-left is-family-softylus-black is-uppercase "><span class="has-text-black">Our</span> <br>Vision</h2>
<div class="column is-6"><p class=" is-family-softylus-light is-5">Bringing the world closer through technology and enhancing our optimal solutions reach around the world, so geographical barriers no longer hold anyone from advancing.
    </p></div></div>
</div>
</div>
</section>
<section class="our-services container " style="margin-bottom: 150px">
    <h2 class=" title px-3 is-2 has-text-black is-family-softylus-black  is-uppercase has-text-centered is-size-3-mobile">Our <span class="has-text-red">Services</span></h2>
    <hr class=" mb-6" style="width:15%;background-color:#CB0202;margin: auto;">
    <div class="columns is-multiline is-mobile px-5">
        <div class="column is-6-tablet is-6-mobile is-3-desktop has-text-centered">
            <figure>
            <img class="serviceIcon image" src="https://s1.softylus.com/wp-content/uploads/2020/12/webDevelopment.svg">
            </figure>
            <h3 style="fon-size:15px !important;"  class="is-family-softylus-black">Web Development</h3>
            <span class="is-family-softylus-hairline">Building websites with various themes</span>
        </div>
        <div class="column is-6-tablet is-6-mobile is-3-desktop has-text-centered">
            <figure>
                <img class="serviceIcon image" src="https://s1.softylus.com/wp-content/uploads/2020/12/mobileDevelopment.svg">
            </figure>
            <h3 style="fon-size:15px !important;" class="is-family-softylus-black">Mobile Development</h3>
            <span class="is-family-softylus-hairline">Building highly responsive mobile apps</span>
        </div>
        <div class="column is-6-tablet is-6-mobile is-3-desktop has-text-centered">
            <figure>
                <img class="serviceIcon image" src="https://s1.softylus.com/wp-content/uploads/2020/12/design.svg">
            </figure>
            <h3  style="fon-size:15px !important;"  class="is-family-softylus-black">Design</h3>
            <span class="is-family-softylus-hairline">Taking Care of the visual aesthetics of your needs</span>
        </div>
        <div class="column is-6-tablet is-6-mobile is-3-desktop has-text-centered">
            <figure>
                <img class="serviceIcon image" src="https://s1.softylus.com/wp-content/uploads/2020/12/digitalMarketing-1.svg">
            </figure>
            <h3 style="fon-size:15px !important;"  class="is-family-softylus-black">Digital Marketing </h3>
            <span class="is-family-softylus-hairline">From content to campaigns, we make the plans happen</span>
        </div>

    </div>
</section>
<section class="hero softylus-team">
<div class="columns py-6-desktop">
    <div class="column pt-6 has-text-centered">
    <h2   class="title is-2  is-family-softylus-bold has-text-white is-uppercase">Meet The Team </h2>
        <p class="is-large px-6 is-family-softylus-light has-text-white">It took more than one mind to create our team and outstanding work to make you pleased
        </p>
</div>
</div>
<div class="container ">
<div class="columns py-5 px-5">
    <div class="column is-one-third has-text-centered">
        <figure class="mt-5" style="display: inline-block;">
            <img style="width: 200px;" class="image" src="https://ik.imagekit.io/softylus/242_FWI1t_g4UlXG.png"/>
        </figure>
        <h3 class="is-family-softylus-black">Hesham KH<span class="is-family-softylus-reg" style="font-size:12px;font-weight:bold;font-style:italic"> - Web Developer</span></h3>
        <span class="is-family-softylus-hairline" style="font-size:12px">For Hesham programming is not only a job but also a game that he loves to take up no matter how challenging it gets. He finds it fun just like chess and tennis and maybe even more. As long as the music is on he’ll be more pumped up than ever.</span>
        <div></div>
    </div>
    <div class="column is-one-third has-text-centered">
        <figure class="mt-5" style="display: inline-block;">
            <img class="image"  style="width: 200px;" src="https://ik.imagekit.io/softylus/imgonline-com-ua-shape-NJfaPvfF47E_T1tltytJEwhB.png"/>
        </figure>
        <h3 class="is-family-softylus-black">Dana Asnan<span class="is-family-softylus-reg" style="font-size:12px;font-weight:bold;font-style:italic"> - Digital Marketing Specialist </span></h3>
        <span class="is-family-softylus-hairline" style="font-size:12px">Although quiet, Dana finds joy in lining up words creatively so that the message is neatly conveyed. When she wants to recharge herself for work she loves to shuffle her whole playlist till her work is done.</span>
        <div></div>
    </div>
    <div class="column is-one-third has-text-centered">
        <figure class="mt-5" style="display: inline-block;">
            <img class="image" style="width: 200px;"  src="https://ik.imagekit.io/softylus/151_hwvcBhDM6nY-.png"/>
        </figure>
        <h3 class="is-family-softylus-black">Abdulrahman Eyad<span class="is-family-softylus-reg" style="font-size:12px;font-weight:bold;font-style:italic"> - Mobile Developer</span></h3>
        <span class="is-family-softylus-hairline" style="font-size:12px">Despite being a night owl, Abdulrahman enjoys seeing live changes and colors popping on the screen while developing mobile apps. You might as well see him enjoying some dark music to help him get his tasks done perfectly as he has zero minutes to waste. He lives by the motto “He who seeks an ending shall never have a start”.</span>
        <div></div>
    </div>

</div></div>

</section>
<section style="margin: 100px auto" class=" container happy-client has-background-white level my-6 ">
    <div class = "columns is-mobile">
        <div class="column has-text-centered is-vcentered is-top-left is-half-desktop is-half-tablet is-full-mobile">
            <figure class="image pt-6-mobile">
                <img style="width: 300px;" src="https://ik.imagekit.io/softylus/softylus-happy-clients_qxS0cEdFhMMX.jpg"/>
            </figure>
        </div>

        <div class="column is-vcentered is-half-desktop is-half-tablet is-full-mobile">
          <div class="is-bottom-right">
              <h2 class="title px-3 is-hidden-tablet has-text-black is-family-softylus-bold is-uppercase is-size-5-mobile has-text-centered-mobile">Happy Clients <span class="has-text-red"> Comments</span>
                <!-- <h2 class="title px-3 is-2 has-text-black is-family-softylus-bold is-uppercase is-hidden-mobile">Happy <br> Clients <br><span class="has-text-red">Comments</span></h2> -->
                <!-- <p class="px-3 is-family-softylus-light is-size-6-mobile has-text-centered-mobile">Arcu non odio euismod lacinia at quis risus sed vulputate Vitae sapien pellentesque habitant morbi </p> -->

          </div>
        </div>
    </div>
    <div class="HRcontainer"></div><hr class="is-hidden-mobile line_diagonal is-hidden-tablet-only">
</section>


   <section  class="section is-hidden-fullhd is-hidden-desktop is-hidden-tablet" style="min-height:160px;" >
       
</section>
   <section  class="section" >
        <div class="container">
            <div class="columns is-centered">
                <div class="column has-text-centered">
                    
         <section   id="testim" class=" section testim">
         <h2 class="title px-3 is-2 has-text-black is-family-softylus-bold is-uppercase is-hidden-mobile">Happy Clients <br><span class="has-text-red">Comments</span></h2>

<!--         <div class="testim-cover"> -->
            <div class="wrap">

                <span id="right-arrow" class="arrow right fa fa-chevron-right"></span>
                <span id="left-arrow" class="arrow left fa fa-chevron-left "></span>
                <ul id="testim-dots" class="dots">
                    <li class="dot active"></li>
                    <li class="dot"></li>
                    <li class="dot"></li>
                    <!-- <li class="dot"></li>
                    <li class="dot"></li> -->
                </ul>
                <div id="testim-content" class="cont">
                    
                    <div class="active">
                        <div class="img"><img  src="https://ik.imagekit.io/softylus/image__1__ky_WZbeiP.png" alt=""></div>
                        <h2 class="is-family-softylus-black">Gildong H.</h2>
                        <p class="is-family-softylus-reg"> Worked with Softylus for the second time, they’re always great and reliable. I Highly recommend them!</p>
                    </div>

                    <div>
                        <div class="img"><img  src="https://ik.imagekit.io/softylus/image_WFcf4w99M0GE.png" alt=""></div>
                        <h2 class="is-family-softylus-black">Abdullah Abu Khalaf </h2>
                        <p class="is-family-softylus-reg">I have to say for a person who's very hard to please, I simply  had the best experience! Very happy that Softylus had done my website project exactly as imagined...they give you their full capacity of work, efforts and dedicated time. HIGHLY RECOMMENDED.</p>
                    </div>

                   <!-- <div>
                       <div class="img"><img  src="https://image.ibb.co/iN3qES/pexels_photo_324658.jpg" alt=""></div>
                       <h2 class="is-family-softylus-black">AHMAD ALMosawi</h2>
                       <p class="is-family-softylus-reg">One of the best companies that I dealt with, they’re really quick and do whatever you want skillfully</p>
                   </div> -->
<!-- 
                   <div>
                       <div class="img"><img src="https://image.ibb.co/kL6AES/Top_SA_Nicky_Oppenheimer.jpg" alt=""></div>
                       <h2 class="is-family-softylus-black">Laura Lopez</h2>
                       <p class="is-family-softylus-reg">Great company and the team is very friendly with such great support. I'm very happy to be one of Softylus customers.</p>
                   </div> -->

                    <div>
                        <div class="img"><img src="https://image.ibb.co/gUPag7/image.jpg" alt=""></div>
                        <h2 class="is-family-softylus-black">Mohannad N.</h2>
                        <p class="is-family-softylus-reg">The best company to work with. Finished on time and were very supportive. I recommend them and will work with them again.</p>
                    </div>

                </div>

            </div>
<!--         </div> -->
    </section>

<!-- <script src="https://use.fontawesome.com/1744f3f671.js"></script> -->

         </div>
        </div>
      </div>
    </section>

  

     <section style="margin-top:200px;" class="section">
        <div class="container">
        <div class="columns is-multiline is-vcentered">
               <!--first 1 column -->
              <div class="column  is-2" >

            </div>
            <div class="column is-vcentered has-text-centered has-text-centered-mobile  is-3" >
                  <p class="has-text-black is-size-3 is-family-softylus-bold"> How It Started?</p>
            </div>
             <div class="column is-vcentered has-text-centered is-2" >
              <img  src="https://s1.softylus.com/wp-content/uploads/2020/12/imgonline-com-ua-shape-QvqlZy1Mn1U.jpg" alt="">
            </div>
             <div class="column is-vcentered has-text-centered-mobile is-3" >
                    <h3 class=" is-size-5 is-family-softylus-bold"  >The Founder</h3>
                    <p class="has-text-left has-text-centered-mobile  is-size-6 is-family-softylus-reg " style="font-size: 13px;padding:5px"> Starting as a freelancer from home, Softylus founder managed to create not only solid connections but also great projects that speak for themselves.</p>
            </div>
             <div class="column  is-2" >

            </div>
              <div class="column has-text-centered is-12" >
           
            </div>
            <!--secound 2 column -->
              <div class="column  is-2" >

            </div>
            <div class="column is-vcentered has-text-centered is-3" >
                 
            </div>
             <div class="column reverse-columns is-2 has-text-centered-mobile  is-vcentered has-text-centered" >
              <img class=""  src="https://s1.softylus.com/wp-content/uploads/2020/12/23.jpg" alt="">
            </div>
             <div class="column  is-vcentered has-text-centered-mobile  is-mobile is-3" >
             <h3 class=" is-size-5 is-family-softylus-bold"  >Softylus</h3>
                    <p class="has-text-left has-text-centered-mobile  is-size-6 is-family-softylus-reg " style="font-size: 13px;padding:5px"> Taking his dream one step further, he decided to create his own business website to offer his services for as many people as he can.</p>

            </div>
             <div class="column  is-2" >

            </div>
              <div class="column has-text-centered is-12" >
 
            </div>
              <!--third 3 column -->
            <div class="column  is-2" >

            </div>
            <div class="column is-hidden-mobile is-vcentered  has-text-centered-mobile  is-3" >
            <h3 class=" is-size-5 is-family-softylus-bold"  >The Office</h3>
                    <p class="has-text-left has-text-centered-mobile  is-size-6 is-family-softylus-reg " style="font-size: 13px;padding:5px"> From building the website to building the office, Softylus found its place in Jordan and started operating.</p>
            </div>
             <div class="column  reverse-columns is-vcentered has-text-centered is-2" >
              <img  src="https://s1.softylus.com/wp-content/uploads/2020/12/223.jpg" alt="">
            </div>
             <div class="column is-vcentered has-text-centered is-3" >
                  
            </div>
             <div class="column is-hidden-fullhd is-hidden-tablet is-vcentered  has-text-centered-mobile  is-3" >
             <h3 class=" is-size-5 is-family-softylus-bold"  >The Office</h3>
                    <p class="has-text-left has-text-centered-mobile  is-size-6 is-family-softylus-reg ">From building the website to building the office, Softylus found its place in Jordan and started operating.</p>

            </div>
             <div class="column  is-2" >

            </div>
              <div class="column has-text-centered is-12" >

         
            </div>
         

            <!--forth 4 column -->
              <div class="column  is-2" >

            </div>
            <div class="column is-vcentered has-text-centered is-3" >
                 
            </div>
             <div class="column is-vcentered has-text-centered is-2" >
              <img  src="https://s1.softylus.com/wp-content/uploads/2020/12/34.jpg" alt="">
            </div>
             <div class="column is-vcentered has-text-centered-mobile  has-text-centered-mobile   is-3" >
             <h3 class=" is-size-5 is-family-softylus-bold"  >The Team</h3>
                    <p class="has-text-left has-text-centered-mobile  is-size-6 is-family-softylus-reg " style="font-size: 13px;padding:5px">The team expanded with more talents teaming up to offer what they’re best at!</p>
            </div>
             <div class="column  is-2" >

            </div>
              <div class="column has-text-centered is-12" >
    
            </div>

            <!--forth 5 column -->
             <div class="column  is-2" >

            </div>
            <div class="column p-2 is-vcentered has-text-centered is-3" >
                  <p class="has-text-black px-3 is-size-3 is-family-softylus-bold"> want to know our biggest achievemnt for 2020?</p>
            </div>
             <div class="column is-vcentered has-text-centered is-2" >
              <img src="https://s1.softylus.com/wp-content/uploads/2020/12/126.jpg" alt="">
            </div>
             <div class="column is-vcentered has-text-centered-mobile  has-text-centered-mobile  is-3" >
             <h3 class=" is-size-5 is-family-softylus-bold"  >Hosting Services</h3>
                    <p class="has-text-left  has-text-centered-mobile  is-size-6 is-family-softylus-reg ">It’s no longer development only! We managed to create our own hosting platform and provide our clients with brilliant solutions.</p>
            </div>
             <div class="column  is-2" >

            </div>
              <div class="column has-text-centered is-12" >
       
            </div>

      </div>
      </div>
    </section>


    <section  class="section">
        <div class="container">
        <div class="columns is-centered">
            <div class="column  has-text-centered">
                <h1 class="is-centered is-family-softylus-black title is-2">OUR <span class="has-text-red">CLIENTS</span></h1>
                <p class="my-5 mx-6 is-family-softylus-reg">Get a look at our clients’ list and and be one of them
                </p>
            </div>
        </div>
        <div class="columns is-multiline is-centered is-mobile">
            <div class="column is-2-desktop  is-two-fifths-mobile is-radiusless mb-2  p-4  box  has-text-centered is-mxy-20">
                <img   class="image transformOnmobile  logo-style p-0 is-128x128 has-text-centered" src="https://ik.imagekit.io/softylus/jopanda_cgrvd94qgm.jpg" alt="">
            </div>
            <div class="column is-2-desktop  is-two-fifths-mobile is-radiusless mb-2  p-4  box  has-text-centered is-mxy-20">
                <img   class="image transformOnmobile  logo-style p-0 is-128x128 has-text-centered" src="https://ik.imagekit.io/softylus/plapiel_16GkuALWt.jpg" alt="">
            </div>

            <div class="column is-2-desktop  is-two-fifths-mobile is-radiusless mb-2  p-4  box  has-text-centered is-mxy-20">
                  <img class="image transformOnmobile  logo-style p-0 is-128x128 has-text-centered" src="https://ik.imagekit.io/softylus/SNS_Cafe_logo_1_PBgXza25NhQ0.jpg" alt="">
            </div>
            <div  class="column is-2-desktop  is-two-fifths-mobile is-radiusless mb-2  p-4  box  has-text-centered is-mxy-20">
                   <img  class="image transformOnmobile logo-style p-0 is-128x128 has-text-centered" src="https://ik.imagekit.io/softylus/webpro-2048x1513-1_VeV7Ap1b11uS_PWC54KJin.jpg" alt="">
            </div>
            <div class="column is-2-desktop  is-two-fifths-mobile is-radiusless mb-2  box  has-text-centered is-mxy-20">
                   <img  class="image transformOnmobile logo-style p-0 is-128x128 has-text-centered" src="https://ik.imagekit.io/softylus/1_z4k235JFI.jpg" alt="">
            </div>
             <div class="column is-2-desktop  is-two-fifths-mobile is-radiusless mb-2  p-4  box has-text-centered is-mxy-20">
                  <img class="image transformOnmobile logo-style p-0 is-128x128 has-text-centered" src="https://ik.imagekit.io/softylus/logo-02_aE7bDxfM8hJV_WI1f6aV8B.jpg" alt="">

            </div>
                   <div class="column is-2-desktop  is-two-fifths-mobile is-radiusless mb-2  p-4  box  has-text-centered is-mxy-20">
                   <img  class="image transformOnmobile logo-style p-0 is-128x128 has-text-centered" src="https://ik.imagekit.io/softylus/cropped-BEnglish-logo-01-1_JISMqhu23.jpg" alt="">
            </div>
            <div class="column is-2-desktop  is-two-fifths-mobile is-radiusless mb-2  p-4  box  has-text-centered is-mxy-20">
                   <img   class="image transformOnmobile logo-style p-0 is-128x128 has-text-centered" src="https://ik.imagekit.io/softylus/siima_2GX7vD27fn.jpg" alt="">
            </div>
            <div class="column is-2-desktop  is-two-fifths-mobile is-radiusless mb-2  p-4 box  has-text-centered is-mxy-20"> 
                  <img class="image transformOnmobile logo-style p-0 is-128x128 has-text-centered" src="https://ik.imagekit.io/softylus/SGC-transparent-2-1-oxtdetdvnvc3lwnjm8gjfnr9gwlypxq9vw4w3vmsx4_wi3VPcg0nh.jpg" alt=""></div>
             <div class="column is-2-desktop  is-two-fifths-mobile is-radiusless mb-2  p-4  box has-text-centered is-mxy-20">
                  <img class="image transformOnmobile logo-style p-0 is-128x128 has-text-centered" src="https://ik.imagekit.io/softylus/Aqarat-Logo-WhiteGold-18_8F6FMNlR21QE.jpg" alt="">
         </div>
       
        </div>
       
</section>
        <section   class="container mt-6">
            <div class = "columns is-vcentered">
            <div class="column has-text-centered ">
                    <h4 class="is-size-4 is-family-softylus-black">Robinson Elie</h4>
                    <h6 class="is-size-7 is-family-softylus-light">CO-FOUNDER</h6>
                        <hr class="is-m-auto" style="width:5%;background-color:#CB0202;margin: auto;">
                    <p class="my-4 mx-2 px-5 is-family-softylus-reg is-size-4">Your reputation is more important than your paycheck,<br> and your integrity <span class="has-text-red">is worth more</span> than your career.</p>
                    <figure class="image">
                        <img class="image is-m-auto" style="width: 250px;" src="https://ik.imagekit.io/softylus/robinson.jpg_7ao0Wl7_uPII.png">
                    </figure>

                </div>
            </div>
        </section>
    </div>
<script
        src="https://code.jquery.com/jquery-3.5.1.slim.min.js"
        integrity="sha256-4+XzXVhsDmqanXGHaHvgh1gMQKX40OUvDEBTu8JcmNs="
        crossorigin="anonymous"></script>
<script src="https://s1.softylus.com/wp-content/themes/softylus/testimonials.js" >
</script>






<?php get_footer(); ?>
