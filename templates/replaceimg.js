   $(function () {
       $(".facebook").on({
           mouseenter: function () {
               $(".facebookImg").attr('src', 'https://ik.imagekit.io/softylus/facebook-logo-in-circular-shape_1_white_ojrLBdCe9-.png');
           },
           mouseleave: function () {
               $(".facebookImg").attr('src', 'https://ik.imagekit.io/softylus/facebook-logo-in-circular-shape_LwG0yai_T.svg');
           }
       });

   });

   $(function () {
       $(".ppc").on({
           mouseenter: function () {
               $(".ppcImg").attr('src', 'https://ik.imagekit.io/softylus/pay-per-click__2__1_white_zoWPRWK0fB.png');
           },
           mouseleave: function () {
               $(".ppcImg").attr('src', 'https://ik.imagekit.io/softylus/pay-per-click__2__cOscI52MxQAH.svg');
           }
       });

   });

   $(function () {
       $(".local").on({
           mouseenter: function () {
               $(".localImg").attr('src', 'https://ik.imagekit.io/softylus/local__2__1_white_e7VwKchSgS.png');
           },
           mouseleave: function () {
               $(".localImg").attr('src', 'https://ik.imagekit.io/softylus/local__2__quyZREeCV.svg');
           }
       });

   });

   $(function () {
       $(".emailM").on({
           mouseenter: function () {
               $(".emailMImg").attr('src', 'https://ik.imagekit.io/softylus/email-marketing__1__1_white_LZk8Zipb13.png');
           },
           mouseleave: function () {
               $(".emailMImg").attr('src', 'https://ik.imagekit.io/softylus/email-marketing__1__9Jkg7cThK.svg');
           }
       });

   });



   $(function () {
       $(".video").on({
           mouseenter: function () {
               $(".videoImg").attr('src', 'https://ik.imagekit.io/softylus/icon_whit_qH_W-Jk_nx.svg');
           },
           mouseleave: function () {
               $(".videoImg").attr('src', 'https://ik.imagekit.io/softylus/nn_5i7p8BfnK2gn.svg');
           }
       });

   });

   $(function () {
       $(".socilaAd").on({
           mouseenter: function () {
               $(".socilaAdImg").attr('src', 'https://ik.imagekit.io/softylus/socilaAd_1_white_jbPM3TPqK4FSe.png');
           },
           mouseleave: function () {
               $(".socilaAdImg").attr('src', 'https://ik.imagekit.io/softylus/socilaAd_Y_-IS8O7zaZ7.svg');
           }
       });

   });

   $(function () {
       $(".contentt").on({
           mouseenter: function () {
               $(".contentImg").attr('src', 'https://ik.imagekit.io/softylus/content_1white_Wtp0YhJgh9.svg');
           },
           mouseleave: function () {
               $(".contentImg").attr('src', 'https://ik.imagekit.io/softylus/content_1red_J7XfxMI_Q.svg');
           }
       });

   });
   $(function () {
       $(".seo").on({
           mouseenter: function () {
               $(".seoImg").attr('src', 'https://ik.imagekit.io/softylus/web_z1MfBIJukOmFj.svg');
           },
           mouseleave: function () {
               $(".seoImg").attr('src', 'https://ik.imagekit.io/softylus/iconnn_RteGeaHlSQrKD.svg');
           }
       });

   });
   $(function () {
       $(".linkedIn").on({
           mouseenter: function () {
               $(".linkedInImg").attr('src', 'https://ik.imagekit.io/softylus/Group_74_1_white_Ch9rFlu7vqzh4.png');
           },
           mouseleave: function () {
               $(".linkedInImg").attr('src', 'https://ik.imagekit.io/softylus/Group_74_zOhfZHd8hhNQY.svg');
           }
       });

   });

   $(function () {
       $(".convertion").on({
           mouseenter: function () {
               $(".convertionImg").attr('src', 'https://ik.imagekit.io/softylus/iconn_1_white_XF7nNPffi-mob.png');
           },
           mouseleave: function () {
               $(".convertionImg").attr('src', 'https://ik.imagekit.io/softylus/iconn_DzdL-Y58R.svg');

           }
       });
   });

   
// end of marketing page js

//features in mobile development page
$(function () {
    $(".performance").on({
        mouseenter: function () {
            $(".performanceImg").attr('src', 'https://s1.softylus.com/wp-content/uploads/2020/12/performance-white.svg');
        },
        mouseleave: function () {
            $(".performanceImg").attr('src', 'https://s1.softylus.com/wp-content/uploads/2020/12/performance2.svg');
        }
    });
});
$(function () {
    $(".secure").on({
        mouseenter: function () {
            $(".secureImg").attr('src', 'https://s1.softylus.com/wp-content/uploads/2020/12/secure-white.svg');
        },
        mouseleave: function () {
            $(".secureImg").attr('src', 'https://s1.softylus.com/wp-content/uploads/2020/12/secure2.svg');
        }
    });
});
$(function () {
    $(".custom").on({
        mouseenter: function () {
            $(".customImg").attr('src', 'https://s1.softylus.com/wp-content/uploads/2020/12/cusomization-white.svg');
        },
        mouseleave: function () {
            $(".customImg").attr('src', 'https://s1.softylus.com/wp-content/uploads/2020/12/cusomization2.svg');
        }
    });
});
$(function () {
    $(".creative").on({
        mouseenter: function () {
            $(".creativeImg").attr('src', 'https://s1.softylus.com/wp-content/uploads/2020/12/creative-display-white.svg');
        },
        mouseleave: function () {
            $(".creativeImg").attr('src', 'https://s1.softylus.com/wp-content/uploads/2020/12/creative-display2.svg');
        }
    });
});
$(function () {
    $(".plan").on({
        mouseenter: function () {
            $(".planImg").attr('src', 'https://ik.imagekit.io/softylus/Group_6_UijNaXpEYR.svg');
        },
        mouseleave: function () {
            $(".planImg").attr('src', 'https://ik.imagekit.io/softylus/Group_4_f_-7nunP4X9L.svg');
        }
    });
});
$(function () {
    $(".creation").on({
        mouseenter: function () {
            $(".creationImg").attr('src', 'https://ik.imagekit.io/softylus/Group_10_GfU8ZG72t3XW.svg');
        },
        mouseleave: function () {
            $(".creationImg").attr('src', 'https://ik.imagekit.io/softylus/Group_5_icBNu696h.svg');
        }
    });
});
$(function () {
    $(".happy").on({
        mouseenter: function () {
            $(".happyImg").attr('src', 'https://ik.imagekit.io/softylus/Group_12_Dlo3d2Cc0fS3.svg');
        },
        mouseleave: function () {
            $(".happyImg").attr('src', 'https://ik.imagekit.io/softylus/Group_2_pjV5mgSyANcDa.svg');
        }
    });
});$(function () {
    $(".lunch").on({
        mouseenter: function () {
            $(".lunchImg").attr('src', 'https://ik.imagekit.io/softylus/Group_11_Jy1MoKNt1.svg');
        },
        mouseleave: function () {
            $(".lunchImg").attr('src', 'https://ik.imagekit.io/softylus/Group_3_kDvpA42NAm3LV.svg');
        }
    });
});
   //features in web development page
   $(function () {
       $(".seo1").on({
           mouseenter: function () {
               $(".seoImg1").attr('src', 'https://s1.softylus.com/wp-content/uploads/2020/12/seo-white.svg');
           },
           mouseleave: function () {
               $(".seoImg1").attr('src', 'https://s1.softylus.com/wp-content/uploads/2020/12/seo-red2.svg');
           }
       });
   });
   $(function () {
       $(".friendly").on({
           mouseenter: function () {
               $(".friendlyImg").attr('src', 'https://s1.softylus.com/wp-content/uploads/2020/12/user-friendly-white.svg');
           },
           mouseleave: function () {
               $(".friendlyImg").attr('src', 'https://s1.softylus.com/wp-content/uploads/2020/12/user-friendly-red2.svg');
           }
       });
   });
   $(function () {
       $(".responsive").on({
           mouseenter: function () {
               $(".responsiveImg").attr('src', 'https://s1.softylus.com/wp-content/uploads/2020/12/responsive-white.svg');
           },
           mouseleave: function () {
               $(".responsiveImg").attr('src', 'https://s1.softylus.com/wp-content/uploads/2020/12/responsive-red2.svg');
           }
       });
   });